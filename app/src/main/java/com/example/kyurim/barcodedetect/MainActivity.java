package com.example.kyurim.barcodedetect;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ImageView
        ImageView myImageView = (ImageView) findViewById(R.id.imgview);
        final Bitmap myBitmap = BitmapFactory.decodeResource(
                getApplicationContext().getResources(),
                R.drawable.isbn_3);
        myImageView.setImageBitmap(myBitmap);

        // Button
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // TextView
                TextView textView = (TextView) findViewById(R.id.txtContent);

                // BarcodeDetector
                BarcodeDetector detector = new BarcodeDetector.Builder(getApplicationContext())
//                        .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)     // this will limit the type of barcodes
                        .build();
                if (!detector.isOperational()) {
                    textView.setText("Could not set up the detector!");
                    return;
                }

                // Detect the Barcode
                Frame frame = new Frame.Builder().setBitmap(myBitmap).build();
                SparseArray<Barcode> barcodes = detector.detect(frame);

                // Decode the Barcode
                // Hardcoded to assume that there is only one barcode.
                Barcode thisCode = barcodes.valueAt(0);     // pick the first barcode only (incase there are more than one)
                textView.setText(thisCode.rawValue);
            }
        });
    }
}
